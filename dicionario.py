
def buscar(alunos, ra):
    if ra in alunos:
        return alunos[ra]
    else:
        raise KeyError

alunos = {}
for i in range(3):
    try:
        ra = int(input('Informe seu RA: '))
        ra = str(ra)
        if len(ra) != 7:
            raise ValueError
        ra = int(ra)
        if ra in alunos:
            raise TypeError
        nome = input('Informe seu nome: ')
        alunos[ra] = nome
    except ValueError:
        print('o RA precisa ter 7 digitos ')
    except TypeError:
        print('O RA informado ja existe')
print(alunos)
try:
    print(buscar(alunos, 1234567))

except KeyError:
    print('O RA cadastrado não está cadastrado')