class Aluno:
    def __init__(self):
        self.nome = ""
        self.tempo_sem_dormir = 0

    def estudar(self, horas):
        self.tempo_sem_dormir = self.tempo_sem_dormir + horas

    def dormir(self, horas):
        self.tempo_sem_dormir = self.tempo_sem_dormir - horas


aluno1 = Aluno()
aluno1.nome = "Paulo"
aluno1.estudar(3)
print("Tempo sem dormir: ", aluno1.tempo_sem_dormir)
aluno1.estudar(1)
print("Tempo sem dormir: ", aluno1.tempo_sem_dormir)
aluno1.dormir(2)
print("Tempo sem dormir: ", aluno1.tempo_sem_dormir)