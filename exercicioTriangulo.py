class Triangulo:
    def __init__(self, a, b, c):
        self.lado_a = a
        self.lado_b = b
        self.lado_c = c

    def calcular_perimetro(self):
        return self.lado_a + self.lado_b + self.lado_c


triangulo1 = Triangulo(10, 14, 15)
p = triangulo1.calcular_perimetro()
print('Perimetro: ', p)

triangulo2 = Triangulo(34, 65, 33)
p = triangulo2.calcular_perimetro()
print('Perimetro: ', p)